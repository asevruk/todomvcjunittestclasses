package net.itlabs;

import net.itlabs.features.ToDoE2E;
import net.itlabs.features.ToDosOperationsAtAllFilter;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by ������ on 13.02.2016.
 */
@RunWith(Categories.class)
@Suite.SuiteClasses({ToDoE2E.class, ToDosOperationsAtAllFilter.class})
public class AllSuiteTest {
}
