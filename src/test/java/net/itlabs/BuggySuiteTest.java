package net.itlabs;

import net.itlabs.categories.Buggy;
import net.itlabs.features.ToDosOperationsAtAllFilter;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by ������ on 13.02.2016.
 */
@RunWith(Categories.class)
@Suite.SuiteClasses(ToDosOperationsAtAllFilter.class)
@Categories.IncludeCategory(Buggy.class)
public class BuggySuiteTest {
}
