package net.itlabs.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static net.itlabs.pages.ToDoMVC.Task.aTask;
import static net.itlabs.pages.ToDoMVC.Type.ACTIVE;

public class ToDoMVC {
    public static ElementsCollection tasks = $$("#todo-list li");
    public static SelenideElement newTask = $("#new-todo");
    public static SelenideElement clearCompltetedButton = $("#clear-completed");

    @Step
    public static void assertVisibleTasksAreEmpty() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public static void assertVisibleTasks(String... tasksTexts) {
        tasks.filter(visible).shouldHave(exactTexts(tasksTexts));
    }

    @Step
    public static void openActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public static void openCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public static void openAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public static void create(String... tasksTexts) {
        for (String text : tasksTexts) {
            newTask.setValue(text).pressEnter();
        }
    }

    @Step
    public static SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    @Step
    public static void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    @Step
    public static void assertItemsLeft(Integer count) {
        $("#todo-count strong").shouldHave(exactText(String.valueOf(count)));
    }

    @Step
    public static void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public static void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    @Step
    public static void clearCompleted() {
        $("#clear-completed").click();
    }

    public static enum Type {ACTIVE, COMPLETED}

    public enum Filter {

        ALL(""), ACTIVE("#/active"), COMPLETED("#/completed");

        Filter(String subURL) {
            this.subUrl = subURL;
        }

        public String subUrl;
    }

    public static class Task {
        String name;
        Type status;

        public Task(String name, Type status) {
            this.name = name;
            this.status = status;
        }

        public static Task aTask(String name, Type status) {
            return new Task(name, status);
        }

        public static Task aTask(String name) {
            return new Task(name, ACTIVE);
        }
    }

    public static void given(Filter filter, Task... tasks) {
        ensureOpenPage(filter);
        String tempResultJS = "localStorage.setItem(\"todos-troopjs\", \"[";

        for (Task task : tasks) {
            tempResultJS += "{\\\"completed\\\":" + (task.status == ACTIVE ? false : true) + ", \\\"title\\\":\\\"" + task.name + "\\\"},";

        }
        if (tasks.length > 0) {
            tempResultJS = tempResultJS.substring(0, tempResultJS.length() - 1) + "]\")";
        } else {
            tempResultJS += "]\")";
        }

        executeJavaScript(tempResultJS);

        executeJavaScript("location.reload()");
    }

    public static Task[] convertToTasks(String... texts) {

        Task[] arrayTask = new Task[texts.length];

        for (int i = 0; i < texts.length; i++) {
            arrayTask[i] = aTask(texts[i], ACTIVE);
        }
        return arrayTask;
    }

    public static void givenAtAll() {
        given(Filter.ALL);
    }

    public static void givenAtAll(Task... tasks) {
        given(Filter.ALL, tasks);
    }

    public static void givenAtAll(String... taskTexts) {
        given(Filter.ALL, convertToTasks(taskTexts));
    }

    public static void givenAtActive() {
        given(Filter.ACTIVE);
    }

    public static void givenAtActive(Task... tasks) {
        given(Filter.ACTIVE, tasks);
    }

    public static void givenAtActive(String... taskTexts) {
        given(Filter.ACTIVE, convertToTasks(taskTexts));
    }

    public static void givenAtCompleted(Task... tasks) {
        given(Filter.COMPLETED, tasks);
    }

    public static void givenCompleted(String... taskTexts) {
        given(Filter.COMPLETED, convertToTasks(taskTexts));
    }

    public static void ensureOpenPage(Filter filter) {
        String toDOMVCUrl = "https://todomvc4tasj.herokuapp.com/";

        if (!(url().equals(toDOMVCUrl + filter.subUrl)))
            open(toDOMVCUrl + filter.subUrl);

    }
}
